public class CompleteLinkage implements ClusterMethod {
	
	DistanceMeasure distanceMeasure;

	CompleteLinkage(DistanceMeasure distanceMeasure) {
		this.distanceMeasure = distanceMeasure;
	}

	public double calculateDistance(Cluster cluster1, Cluster cluster2) {
		double maxValue = -Double.MAX_VALUE;
	
		for(int i = 0 ; i < cluster1.getWidth() ; i++) {
			for(int j = 0 ; j < cluster2.getWidth() ; j++) {
				if(maxValue <  distanceMeasure.calculateDistance(cluster1.getUnits().getUnit(i), cluster2.getUnits().getUnit(j))) {
					maxValue = distanceMeasure.calculateDistance(cluster1.getUnits().getUnit(i), cluster2.getUnits().getUnit(j));
				}
			}
		}
		return maxValue;
	}

}
