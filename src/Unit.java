public class Unit {

	private String elementName;
	private NumberRow numberRow;
	
	Unit(String elementName) {
		this.elementName = elementName;
	}
	
	public String getElementName() {
		return elementName;
	}
	
	public void addNumberRow(NumberRow numberRow) {
		this.numberRow = numberRow;
	}
	
	public double getValue(int i) {
		return numberRow.getValue(i);
	}
	
	public void resetValue(int i, double value) {
		numberRow.changeValue(i, value);
	}
	
	public int getNumberOfVariables() {
		return numberRow.getLength();
	}
	
}
