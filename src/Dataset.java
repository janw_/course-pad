import java.util.Arrays;

public class Dataset {
	
	final static int VARIABLES_FOR_PRESELECTION = 50;
	
	private int numberOfClusters;
	private int numberOfElements;
	private int numberOfVariables;
	
	private String elementType;
	private String[] variableNames;
	private int counter;
	
	private UnitRow elements;

	Dataset() {
		numberOfClusters = -1;
		numberOfElements = -1;
		numberOfVariables = -1;
		
		elementType = null;
		variableNames = null;
		counter = 0;
				
		elements = null;
	}

	public int getNumberOfClusters() {
		return numberOfClusters;
	}

	public void setNumberOfClusters(int numberOfClusters) {
		this.numberOfClusters = numberOfClusters;
	}

	public int getNumberOfElements() {
		return numberOfElements;
	}

	public void setNumberOfElements(int numberOfElements) {
		this.numberOfElements = numberOfElements;
	}

	public int getNumberOfVariables() {
		return numberOfVariables;
	}

	public void setNumberOfVariables(int numberOfVariables) {
		this.numberOfVariables = numberOfVariables;
	}

	public String getElementType() {
		return elementType;
	}

	public void setElementType(String elementType) {
		this.elementType = elementType;
	}

	public String getVariableName(int i) {
		return variableNames[i];
	}
	
	public void addToVariableName(String variableName) {
		
		if (variableNames == null) {
			variableNames = new String[numberOfVariables];
		}
		
		this.variableNames[counter] = variableName;
		counter += 1;
	}

	public void setUnitRow(UnitRow unitRow) {
		elements = unitRow;
	}
	
	public Unit getUnit(int i) {
		return elements.getUnit(i);
	}
	
	public void normalization() {
		double maxValue;
		double minValue;
		
		for (int value = 0; value < numberOfVariables ; value++) {
			maxValue = getMaximumValue(value);
			minValue = getMinimumValue(value);
			
			for(int unit = 0; unit < numberOfElements ; unit++) {
			
				double normValue = (elements.getUnit(unit).getValue(value) - minValue) / (maxValue - minValue);
				elements.getUnit(unit).resetValue(value, normValue);
			}
		}
	}
	
	public double getMaximumValue(int index) {
		double maxValue = -Double.MAX_VALUE;
		
		for(int unit = 0; unit < numberOfElements; unit++) {
			double tempValue = elements.getUnit(unit).getValue(index);
			
			if(maxValue < tempValue) {
				maxValue = tempValue;
			}
		}
		return maxValue;
	}
	
	public double getMinimumValue(int index) {
		double minValue = Double.MAX_VALUE;
		
		for(int unit = 0; unit < numberOfElements; unit++) {
			double tempValue = elements.getUnit(unit).getValue(index);
			
			if(minValue > tempValue) {
				minValue = tempValue;
			}
		}
		return minValue;
	}

	public void preselection() {
		if (numberOfVariables <= VARIABLES_FOR_PRESELECTION) {
			return;
		}
		
		double[] deviationValues = calculateStandardDeviation();
		int[] indexesOfFiftyHighestValues = calculateFiftyHighestDeviationIndexes(deviationValues);
		updateVariableNames(indexesOfFiftyHighestValues);
		updateVariableValues(indexesOfFiftyHighestValues);
		updateNumberOfVariables();
	}
		
	public double[] calculateStandardDeviation() {
		double[] meanValues = calculateMeans();
		return calculateDeviations(meanValues);
	}
	
	public double[] calculateMeans() {
		double[] meanValues = new double[numberOfVariables];
		
		for (int value = 0; value < numberOfVariables ; value++) {
			for(int unit = 0; unit < numberOfElements ; unit++) {
			
				meanValues[value] += elements.getUnit(unit).getValue(value) / numberOfElements;
			}
		}
		
		return meanValues;
	}
		
	double[] calculateDeviations(double[] meanValues) {
		double[] deviationValues = new double[numberOfVariables];
		
		for (int value = 0; value < numberOfVariables ; value++) {
			for(int unit = 0; unit < numberOfElements ; unit++) {
			
				deviationValues[value] += (elements.getUnit(unit).getValue(value) - meanValues[value]) *
						 (elements.getUnit(unit).getValue(value) - meanValues[value]);
			}
			deviationValues[value] = Math.sqrt(deviationValues[value] / (double) (numberOfElements - 1));
		}
		return deviationValues;
	}
		
	public int[] calculateFiftyHighestDeviationIndexes(double[] deviationValues) {
		int[] result = new int[VARIABLES_FOR_PRESELECTION];
		
		for(int loop = 0 ; loop < VARIABLES_FOR_PRESELECTION ; loop++) {
			double tempHighest = -Double.MAX_VALUE;
			int key = -1;
			for(int value = 0 ; value < numberOfVariables ; value++) {
				if (deviationValues[value] > tempHighest) {
					key = value;
					tempHighest = deviationValues[value];
				}
			}
			deviationValues[key] = 0.0;
			result[loop] = key;
		}
		Arrays.sort(result);
		return result;
	}
	
	public void updateVariableNames(int[] indexesOfFiftyHighestValues) {
		String[] updatedVariableNames = new String[VARIABLES_FOR_PRESELECTION];
		
		for(int var = 0 ; var < VARIABLES_FOR_PRESELECTION ; var++) {
			updatedVariableNames[var] = variableNames[indexesOfFiftyHighestValues[var]];
		}
		
		variableNames = updatedVariableNames;
	}
	
	public void updateVariableValues(int[] indexesOfFiftyHighestValues) {

		for(int unit = 0 ; unit < numberOfElements ; unit++) {
			NumberRow tempNumberRow = new NumberRow(VARIABLES_FOR_PRESELECTION);
			
			for(int var = 0 ; var < VARIABLES_FOR_PRESELECTION ; var++) {
				tempNumberRow.addValue(elements.getUnit(unit).getValue(indexesOfFiftyHighestValues[var]));
			}
			
			elements.getUnit(unit).addNumberRow(tempNumberRow);
		}
	}
	
	public void updateNumberOfVariables() {
		numberOfVariables = VARIABLES_FOR_PRESELECTION;
	}

}
