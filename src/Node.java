public class Node implements Cluster {
	
	Cluster cr1;
	Cluster cr2;
	
	Node(Cluster cr1, Cluster cr2) {
		this.cr1 = cr1;
		this.cr2 = cr2;
	}
	
	public int getDepth() {
		return Math.max(cr1.getDepth(), cr2.getDepth()) + 1;
	}
	
	public int getWidth() {
		return cr1.getWidth() + cr2.getWidth();
	}
	
	public UnitRow getUnits() {
		UnitRow unitRow = new UnitRow(getWidth());
		
		unitRow.addUnitRow(cr1.getUnits());
		unitRow.addUnitRow(cr2.getUnits());
		
		return unitRow;
	}

	public boolean hasChildren() {
		return true;
	}
}
